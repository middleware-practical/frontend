import './App.css';
import io from 'socket.io-client';
import { useEffect, useState } from 'react';
import { DatePicker, Input, Space, Table, Tag } from "antd"
import Column from 'antd/es/table/Column';
import ColumnGroup from 'antd/es/table/ColumnGroup';
import config from './config';

function App() {
  const [data, setData, loading, setStartTime, setEndDate, setSearch] = useFetch()
  useEffect(() => {
    handleSocket();
  }, []);

  const handleSocket = () => {
    try {
      const lobby = io(config.SERVER, {
        transports: ["websocket"],
        forceNew: true,
        upgrade: false
      });
      lobby.on('connection', (socket) => {
        socket.on('disconnect',  () => {
          console.log('disconnected: ', socket);
        });

      });
      lobby.emit("subscribe", "user")
      lobby.on("error", err => {
        console.log(err); 
      })
      lobby.on('push_data',  (msg) => {
        setData(oldArray => [JSON.parse(msg),...oldArray] );
      });
    } catch (error) {
      console.log("err", error)
    }


  };
  useEffect(() => {
    const obj = {}
    const splitted = window.location.search.split("?")
    if (splitted && splitted.length) {
      splitted.map(it => {
        if (it) {
          const split = it.split("=")
          obj[split[0]] = split[1]
        }
      })
    }
  })
  const onChange = (value, dateString) => {
    if (value) setStartTime(new Date(value).getTime())
    else setStartTime()

  };

  const onChangeEnd = (value, dateString) => {
    if (value) setEndDate(new Date(value).getTime())
    else setEndDate()

  };

  return (
    <div className="App">
      <Space direction="vertical">
        <Input.Search onSearch={(val) => setSearch(val)} placeholder='Search'></Input.Search>
        <DatePicker showTime onChange={onChange} placeholder='select start time' />
        <DatePicker showTime onChange={onChangeEnd} placeholder='select end time' />
      </Space>
      <Table dataSource={data} loading={loading} bordered style={{
        width: "100%"
      }} rowKey={Math.random()}>
        <Column title="Time" dataIndex="time" key="time" />
        <ColumnGroup title="Log">
          <Column title="Body" dataIndex="body" key="body" render={(_, record) => {
            return record.log.body
          }} />
          <Column title="Service" dataIndex="service" key="service" render={(_, record) => (
            <>
              {record.log.service && <Tag color="blue" key={record.log.service}>
                {record.log.service}
              </Tag>}
            </>
          )} />
          <Column title="Severity" dataIndex="severity" key="severity" render={(_, record) => {
            return record.log.severity
          }} />
        </ColumnGroup>
      </Table>
    </div>
  );
}

export default App;

function useFetch() {
  const [data, setData] = useState(0)
  const [loading, setLoading] = useState(false)
  const [startDate, setStartTime] = useState()
  const [search, setSearch] = useState()
  const [endDate, setEndDate] = useState()
  useEffect(() => {
    setLoading(true)
    let obj = {}
    if (startDate) {
      obj.startDate = startDate
    }
    if (endDate) {
      obj.endDate = endDate
    }
    if (search) {
      obj.text = search
    }
    fetch(`${config.SERVER}/ingest?` + new URLSearchParams(obj), {
      method: 'GET',

    }).then(res => res.json())
      .then(data => {
        setLoading(false)
        setData(data.data)
      }).catch(err => {
        setLoading(false)
      })
  }, [search, startDate, endDate])


  return [data, setData, loading, setStartTime, setEndDate, setSearch]
}


